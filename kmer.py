__author__ = 'Bright'

outp = open('output.txt','w')
inp = open('rosalind_kmer.txt')

def EnumRecur(n,arrl,pool):
    global Enumnum,Enum
    for i in range(len(pool)):
        a = pool[i]
        if n == 1:
            temp1 = [i for i in arrl]
            temp1.append(a)
            if not temp1 in Enum:
                Enum.append(''.join(temp1))
                Enumnum += 1
        elif n > 1:
            temp1 = [i for i in arrl]
            temp1.append(a)
            EnumRecur(n-1,temp1,pool)

Enumnum = 0
Enum = []
EnumRecur(4,[],'ACGT')
inp.readline()
dna = ''
buf = inp.readline().strip()
while buf:
    dna += buf
    buf = inp.readline().strip()
print dna
kmer = [ 0 for x in range(len(Enum))]
for i in xrange(len(dna)-3):
    if dna[i:i+4] in Enum:
        kmer[Enum.index(dna[i:i+4])] += 1
out = ''
for i in kmer:
    out += str(i) + ' '
print out