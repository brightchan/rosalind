__author__ = 'Bright'

import math

inp = open('rosalind_prob.txt','r')

def ProbRandomStr(l,gc):
    prob = [ 0 for x in range(len(gc))]
    for i in xrange(len(l)):
        if l[i] == 'A' or l[i] == 'T':
            for j in xrange(len(gc)):
                prob[j] += math.log10((1-gc[j])/2)
        elif l[i] == 'C' or l[i] == 'G':
            for j in xrange(len(gc)):
                prob[j] += math.log10(gc[j]/2)
    return prob

seq = inp.readline().strip()
GC = [float(x) for x in inp.readline().strip().split()]

print seq,GC
s = ''
for i in ProbRandomStr(seq,GC):
    s += str(round(i,3)) + ' '
print s

inp.close()