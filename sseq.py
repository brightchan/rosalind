inp = open('rosalind_sseq.txt','r')
outp = open('output.txt','w')

def FASTAread(Inp):
    global FASTA
    i = 0
    buf = Inp.readline().rstrip()
    while buf:
        FASTA.append('')
        buf = Inp.readline().rstrip()
        while not buf.startswith('>') and buf:
            FASTA[i] += buf
            buf = Inp.readline().rstrip()
        i += 1

if __name__ =='__main__':
    FASTA = []
    FASTAread(inp)
    p = []
    k = 0
    for i in xrange(0,len(FASTA[1])):
        for j in xrange(k,len(FASTA[0])):
            if FASTA[1][i] == FASTA[0][j]:
                p.append(str(j+1))
                k = j+1
                break
    print ' '.join(p)

    
inp.close()
outp.close()
