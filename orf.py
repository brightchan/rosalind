inp = open('rosalind_orf.txt','r')
outp = open('output.txt','w')
codon = open('codon dna.txt','r')

c = codon.read().strip().split()
d = dict(zip(c[0::2],c[1::2]))

def translate(rna):
    s = ''
    for i in range(0,len(rna),3):
        s += d[rna[i:i+3]]
    return s

def findORF(rna):
    dic = {}
    for i in xrange(len(rna)):
        if rna[i:i+3] == 'ATG':
            for j in xrange(i+3,len(rna)+1,3):
                if rna[j:j+3] == 'TAA' or rna[j:j+3] == 'TAG' or rna[j:j+3] == 'TGA':
                    dic[translate(rna[i:j])] = 1
                    break
    return dic           

def revcom(DNA):
    l = ''
    for i in range(len(DNA)-1,-1,-1):
        if DNA[i] == 'A':
            l +=('T')
        if DNA[i] == 'T':
            l +=('A')
        if DNA[i] == 'C':
            l +=('G')
        if DNA[i] == 'G':
            l +=('C')
    return l

if __name__ =='__main__':
    RNA = ''
    for d in inp.read().strip().split()[1:]:
    RNA += d 
    ORFa = findORF(RNA)    
    RNA = revcom(RNA)
    ORFb = findORF(RNA)
    orf = dict(ORFa.items()+ORFb.items())
    for d,v in orf.items():
        print d

inp.close()
outp.close()
codon.close()
