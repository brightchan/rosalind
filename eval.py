__author__ = 'Bright'

inp = open('rosalind_eval.txt','r')

length = int(inp.readline().strip())
seq = inp.readline().strip()
GC = [float(x) for x in inp.readline().strip().split()]

prob = [1 for x in range(len(GC))]
for i in range(len(seq)):
    if seq[i] == 'A' or seq[i] == 'T':
        for j in xrange(len(GC)):
            prob[j] *= (1-GC[j])/2
    elif seq[i] == 'C' or seq[i] == 'G':
        for j in xrange(len(GC)):
            prob[j] *= GC[j]/2
sum = [x*(length - len(seq)+1) for x in prob]
out = ''
for i in sum:
    print '{:.3f}'.format(i),

inp.close()