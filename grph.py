inp = open('rosalind_grph.txt','r')
outp = open('output.txt','w')
k = 5
d = {}
graph = []
#dict of seq
buf = inp.readline().rstrip()
while buf:
    name, d[buf[1:]] = buf[1:], ''
    buf = inp.readline().rstrip()
    while not buf.startswith('>') and buf:
        d[name] += buf
        buf = inp.readline().rstrip()
#find adjacency by 2 loops
for name,seq in d.items():
    for name2,seq2 in d.items():
        if name != name2 and seq[-k:] == seq2[:k]:
            graph.append(name+' '+name2)
            outp.write(name+' '+name2+'\n')
print graph
inp.close()
outp.close()
