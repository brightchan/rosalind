inp = open('rosalind_revp.txt','r')
outp = open('output.txt','w') 

def revcom(DNA):
    l = ''
    for i in range(len(DNA)-1,-1,-1):
        if DNA[i] == 'A':
            l +=('T')
        if DNA[i] == 'T':
            l +=('A')
        if DNA[i] == 'C':
            l +=('G')
        if DNA[i] == 'G':
            l +=('C')
    return l

if __name__ =='__main__':
    DNA = ''
    p = []
    for d in inp.read().strip().split()[1:]:
        DNA += d
    print DNA
    for i in xrange(len(DNA)):
        for x in xrange(2,13,1):
            if DNA[i:i+x] == revcom(DNA[i+x:i+2*x]):
                p.append(str(i+1)+' '+str(2*x))
    for i in p:
        print i
                
inp.close()
outp.close()

