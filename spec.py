__author__ = 'Bright'
inp = open('rosalind_spec.txt','r')
isomass = open('Monoisotopic mass table.txt','r')
L = [float(i) for i in inp.readlines()]
m = isomass.read().strip().split()
d = dict(zip([round(float(i),2) for i in m[1::2]],m[0::2]))
prt = ''
for i in range(1,len(L),1):
    prt += d[round(L[i]-L[i-1],2)]
print prt