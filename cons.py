inp = open('rosalind_cons1.txt','r')
outp = open('output.txt','w')
buf = inp.readline().strip()
cons = [[0 for col in range(1001)] for row in range(4)]
while buf:
    seq_n, seq = buf[1:],''
    buf = inp.readline().strip()
    while buf and not buf[0]=='>':
        seq = seq + buf
        buf = inp.readline().strip()
    for i in range(len(seq)):
        if seq[i] == 'A':
            cons[0][i] += 1
        elif seq[i] == 'T':
            cons[1][i] += 1
        elif seq[i] == 'C':
            cons[2][i] += 1
        elif seq[i] == 'G':
            cons[3][i] += 1
s = ''
i,j = 0,0
while i < len(seq):
    j = 0
    maxi = 0
    pos = 0
    while j < 4:
        if cons[j][i] > maxi:
            maxi = cons[j][i]
            pos = j
        j += 1
    if pos == 0:
        s += 'A'
    elif pos ==1:
        s += 'T'
    elif pos ==2:
        s += 'C'
    elif pos ==3:
        s += 'G'
    i += 1
print s
s,i = 'A: ', 0
while i< len(seq):
    s += str(cons[0][i]) + ' '
    i += 1
print s
s,i = 'C: ', 0
while i< len(seq):
    s += str(cons[2][i]) + ' '
    i += 1
print s
s,i = 'G: ', 0
while i< len(seq):
    s += str(cons[3][i]) + ' '
    i += 1
print s
s,i = 'T: ', 0
while i< len(seq):
    s += str(cons[1][i]) + ' '
    i += 1
print s
inp.close()
outp.close()
