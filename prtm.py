inp = open('rosalind_prtm.txt','r')
outp = open('output.txt','w')
isomass = open('Monoisotopic mass table.txt','r')

m = isomass.read().strip().split()
d = dict(zip(m[0::2],m[1::2]))
def prtmass(PROT):
    mass = 0
    for i in xrange(len(PROT)):
        mass += float(d[PROT[i]])
    return round(mass,3)

if __name__ =='__main__':
    prt = inp.read().strip()
    print prtmass(prt)
    
inp.close()
outp.close()
isomass.close()
