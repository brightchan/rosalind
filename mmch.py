# -*- coding: utf-8 -*-
"""
Created on Sun Jun 21 19:07:35 2015

@author: Bright
"""

from math import factorial as fac
rna = 'UCAGACUUUGGCAAUUGAGGCGGCGAGCUCUGCUGUUUGCAGUCAGCAGGAUCACUAGAGCAUAGUACCCGUGUUAGUACACCAGACUUAUGAGCAUUU'
a,b = sorted([rna.count("C"),rna.count("G")])
c,d = sorted([rna.count("A"),rna.count("U")])
p = (fac(b)//fac(b-a)*fac(d)//fac(d-c))
print(p)
