__author__ = 'Bright'

outp = open('output.txt','w')
inp = open('rosalind_lexf (1).txt')


Enumnum = 0
Enum = []

def EnumRecur(n,arrl,pool):
    global Enumnum,Enum
    for i in range(len(pool)):
        a = pool[i]
        if n == 1:
            temp1 = [i for i in arrl]
            temp1.append(a)
            if not temp1 in Enum:
                Enum.append(''.join(temp1))
                Enumnum += 1
        elif n > 1:
            temp1 = [i for i in arrl]
            temp1.append(a)
            EnumRecur(n-1,temp1,pool)


if __name__ =='__main__':
    sym = inp.readline().strip().split()
    length = int(inp.readline().strip())
    EnumRecur(length,[],sym)
    print Enumnum
    for i in Enum:
        outp.write(i+'\n')


outp.close()

