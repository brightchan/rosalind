# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 12:55:19 2015

@author: Bright
"""

import scipy as sc

#with open('rosalind_aspc.txt') as inp:
#    l = [int(i) for i in inp.readline().strip().split()]
n = 1639
k = 1281
s = 0
for i in range(k,n+1):
    s = (s + sc.misc.comb(n, i, exact=True)) % 1000000

print(s)
    