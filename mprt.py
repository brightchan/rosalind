import urllib
inp = open('rosalind_mprt.txt','r')
outp = open('output.txt','w')

def getseq (ID):
    seq = ''
    url = 'http://www.uniprot.org/uniprot/'
    url = url + ID + '.fasta'
    s = urllib.urlopen(url).read().strip().split('\n')
    for i in xrange(1,len(s)):
        seq += s[i]
    return seq

def match (s,m):
    c = ''
    if m[0] == '{':
        for i in xrange(1,len(m)):
            c += m[i]
        if s not in c:
            return True
    elif m[0] == '[':
        for i in xrange(1,len(m)):
            c += m[i]
        if s in c:
            return True
    elif s == m:
        return True

def idmotif (s,m):
    p = ''
    for i in xrange(len(s)):
        j = i
        k = 0
        while match(s[j],m[k]):
                j += 1
                k += 1
                if k == len(m) or j == len(s):
                    break
        if k == len(m):
            p = p + str(i+1)+ ' '
    return p

motif = ['N','{P}','[ST]','{P}']
IDs = inp.read().strip().split()

if __name__ == '__main__':
    for i in xrange(len(IDs)):
        pos = idmotif(getseq(IDs[i]),motif)
        if pos:
            print IDs[i]
            s = ''
            for j in xrange(len(pos)):
                s += pos[j]
            print s

inp.close()
outp.close()
