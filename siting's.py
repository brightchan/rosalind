__author__ = 'Bright'

from xlrd import *
from operator import itemgetter

wb = open_workbook('1.xlsx')
ws = wb.sheet_names()

def uni_prot(ws1,ws2): #find different proteins btw two worksheets and return list of protein names and their %Cov(95)
    if not ws1 in ws or not ws2 in ws:
        return 'Wrong input'
    else:
        cov1 = wb.sheet_by_name(ws1).col_values(5,1)
        prot1 = wb.sheet_by_name(ws1).col_values(6,1)
        cov2 = wb.sheet_by_name(ws2).col_values(5,1)
        prot2 = wb.sheet_by_name(ws2).col_values(6,1)
        uniws1 = []
        uniws2 = []
        for p in prot1:
            if p not in prot2:
                uniws1.append([p,cov1[prot1.index(p)]])
        for p in prot2:
            if p not in prot1:
                uniws2.append([p,cov2[prot2.index(p)]])
        uniws1=sorted(uniws1,key=lambda x: x[1])
        uniws2=sorted(uniws2,key=lambda x: x[1])
        ows1 = open(ws1+'-'+ws2 + '.csv','w')
        ows1.write('sep=\t\n')
        for p in uniws1:
            ows1.write(p[0] + '\t' +str(p[1]) + '\n')
        ows1.close()
        ows2 = open(ws2+'-'+ws1 + '.csv','w')
        ows2.write('sep=\t\n')
        for p in uniws2:
            ows2.write(p[0] + '\t' +str(p[1]) + '\n')
        ows2.close()
        return 'Done'
    
while True:
    print('Please specify the two worksheets to be compared (separated by space).')
    s = input().strip().split()
    print(uni_prot(s[0],s[1]))
    
