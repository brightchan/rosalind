inp = open('rosalind_lia.txt','r')
outp = open('output.txt','w')

def AaBb(k,N):
    prob = 0
    s = 2 ** k
    p = 0.25
    for i in xrange(N,s+1,1):
        prob += C(s,i)*(p ** i)*((1-p)**(s-i))
    return round(prob,3)
    
def C(n,k):
    return Factorial(n)/(Factorial(k)*Factorial(n-k))

def Factorial(n):
    p = 1.0
    for i in xrange(n):
        p *= i+1
    return p

print AaBb(5,9)

inp.close()
outp.close()


