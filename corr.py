# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 23:15:51 2015

@author: Bright
"""

#from Tools import read_FASTA,revcom
#import time
#
#with open('rosalind_corr.txt','r') as inp:
#    seq = read_FASTA(inp)
#
##if hamming distance btw s1,s2 >2 then return 3
#def hamm(s1,s2):
#    count = 0
#    k = 0
#    while k < len(i) and count <= 2:
#        if s1[k] != s2[k]:
#            count += 1
#        k += 1
#    return count
#
##a bag to sort similar seq into one list with occurance w/o revcom seq
#start = time.time()
#bag = [{seq[0]:1}]
#for i in seq[1:]:
#    flag = False
#    j = 0
#    while j<len(bag) and not flag:
#        if i in bag[j] or revcom(i) in bag[j]:
#            bag[j][i] += 1
#            flag = True
#        else:
#            for k,v in bag[j].items():
#                if hamm(i,k)<3:
#                    bag[j][i] = 1
#                    flag = True
#                    break
#                if hamm(revcom(i),k)<3:
#                    bag[j][i] = 1
#                    flag = True
#                    break
#        j += 1
#    if not flag:
#        bag.append({i:1})
#
#with open('output.txt','w') as outp:        
#    for i in bag:
#        sorted_i = sorted(i.items(), key=lambda x: x[1], reverse=True)
#        cor_i = sorted_i[0][0]
#        for j in sorted_i[1:]:
#            outp.write(j[0] + '->' + cor_i + '\n')
#end = time.time()
#print(start-end)



start = time.time()

def revcomp(s):
    return s.translate(str.maketrans('ACTG','TGAC'))[::-1]

def hamming(s,t):
    return len([x for x, y in zip(s,t) if x != y])

def include_revcomp(reads):
    return reads[:] + [ revcomp(read) for read in reads]

reads = seq
reads_and_revcomp = include_revcomp(reads)
correct_reads = set( include_revcomp( [ read for read in reads if reads_and_revcomp.count(read) > 1 ] ) )
print(correct_reads)
res=""
for read in (set(reads) - correct_reads):
    print("%s->%s\n" % (read, [ r2 for r2 in correct_reads if hamming(r2,read) == 1 ][0]))
end = time.time()
print(start-end)