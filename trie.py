# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 20:10:12 2015

@author: Bright

Copy from jschendel github
whole trie class with node_num begins at 1
"""

class Trie:
    def __init__(self, words):
        self.create_node = lambda p,d : {'parent':p, 'children':[], 'depth':d, 
                                         'end': False}
        self.nodes = {1:self.create_node(0,0)}
        self.edges = {}
        
        if type(words) is str:
            self._add_word(words)
        else:
            for word in words:
                self._add_word(word)
    
    def _add_word(self, current_word):        
        insertion_node, insertion_substring = self._insert_location(current_word)
        
        for i in range(len(insertion_substring)):
            new_node = len(self.nodes) + 1
            self.nodes[new_node] = self.create_node(insertion_node, self.nodes[
                                                   insertion_node]['depth']+1)
            self.nodes[insertion_node]['children'].append(new_node)
            self.edges[insertion_node, new_node] = insertion_substring[i]
            insertion_node = new_node

        self.nodes[insertion_node]['end'] = True
        
    def _insert_location(self, word_to_add, current_node=1):
        if word_to_add =='':
            return current_node, word_to_add
        
        for child_node in self.nodes[current_node]['children']:
            if self.edges[current_node, child_node] == word_to_add[0]:
                return self._insert_location(word_to_add[1:], child_node)
        
        return current_node, word_to_add
        
    def word_up_to_node(self, node_num):
        node_word = ''
        while self.nodes[node_num]['parent'] != 0:
            node_word += self.edges[self.nodes[node_num]['parent'], node_num]
            node_num = self.nodes[nudes_num]['parent']
        return node_word[::-1]    
        
    def prefix_in_trie(self, word_to_check, current_node=1):
        if self.nodes[current_node]['end'] is True:
            return True
        elif word_to_check == '':
            return False
        for child_node in self.nodes[current_node]['children']:
            if self.edges[current_node, child_node] == word_to_check[0]:
                return self.prefix_in_trie(word_to_check[1:], child_node)
        return False
    
    
if __name__ == '__main__' :
    with open('rosalind_trie.txt') as inp:
        dna = [l.strip() for l in inp.readlines()]
    dna_trie = Trie(dna)    
    adj_list = [' '.join([str(k[0]),str(k[1]),v]) for (k, v) in dna_trie.edges.items()]
    with open('output.txt','w') as out:
        out.write('\n'.join(adj_list))
#    n = [str(k) + ' ' + ' '.join([str(j) for (i,j) in v.items()]) for (k,v) in dna_trie.nodes.items()]
#    print('\n'.join(n))