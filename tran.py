inp = open('rosalind_tran.txt','r')
outp = open('output.txt','w')

def FASTAread(Inp):
    global FASTA
    i = 0
    buf = Inp.readline().rstrip()
    while buf:
        FASTA.append('')
        buf = Inp.readline().rstrip()
        while not buf.startswith('>') and buf:
            FASTA[i] += buf
            buf = Inp.readline().rstrip()
        i += 1

def TransiToTransvRatio(s1,s2):
    ts = 0.0
    tv = 0.0
    for i in xrange(len(s1)):
        if s1[i] <> s2[i]:
            buf = s1[i] + s2[i]
            if ('A' in buf and 'G' in buf) or ('C' in buf and 'T' in buf):
                ts += 1
            else:
                tv += 1
    return ts/tv

if __name__ =='__main__':
    FASTA = []
    FASTAread(inp)
    print TransiToTransvRatio(FASTA[0],FASTA[1])

    
inp.close()
outp.close()
