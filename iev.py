inp = open('rosalind_iev.txt','r')
outp = open('output.txt','w')
a,b,c,d,e,f = (float(x) for x in inp.readline().strip().split())
p = 2*((a+b+c) + 3*d/4 + e/2)
print p
inp.close()
outp.close()

displayDominant = [1.0, 1.0, 1.0, 0.75, 0.5, 0.0]
offspring = 2

with open(filepath) as file:
    parentCounts = [int(x) for x in file.read().split()]

print sum([offspring * x[0] * x[1] for x in zip(displayDominant, parentCounts)])
