inp = open('rosalind_splc.txt','r')
outp = open('output.txt','w')
codon = open('codon dna.txt','r')

c = codon.read().strip().split()
d = dict(zip(c[0::2],c[1::2]))

def translate(dna):
    s = ''
    for i in range(0,len(dna),3):
        s += d[dna[i:i+3]]
    return s


def FASTAread(Inp):
    global FASTA
    i = 0
    buf = Inp.readline().rstrip()
    while buf:
        FASTA.append('')
        buf = Inp.readline().rstrip()
        while not buf.startswith('>') and buf:
            FASTA[i] += buf
            buf = Inp.readline().rstrip()
        i += 1

if __name__ =='__main__':
    FASTA = []
    FASTAread(inp)
    buf = FASTA[0]
    for i in xrange(1,len(FASTA)):
        buf = buf.replace(FASTA[i],'')
    print translate(buf)

    
inp.close()
outp.close()
codon.close()
