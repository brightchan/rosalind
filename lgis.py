__author__ = 'Bright'
import time


inp = open('rosalind_lgis.txt')
outp = open('output.txt','w')

def LongSub(n):#n:position
    ## Increase
    rem = [ListIn[i] for i in xrange(n,len(s)) if s[i] > s[n]]
    if rem:
        ListIn[n] = (max(rem)[0]+1,[n]+max(rem)[1])
    else:
        ListIn[n] = (1,[n])
    rem = [ListDe[i] for i in xrange(n,len(s)) if s[i] < s[n]]
    if rem:
        ListDe[n] = (max(rem)[0]+1,[n]+max(rem)[1])
    else:
        ListDe[n] = (1,[n])

    '''
    ## Decrease
    rem = [i for i in xrange(n,len(s)) if s[i] < s[n]]
    longestDe = []
    lengthDe = 0
    if rem:
        for i in rem:
            l = ListDe[i][0:]
            if len(l) > lengthDe:
                lengthDe = len(l)
                longestDe = l
        ListDe[n] = [n] + longestDe
    else:
        ListDe[n] = [n]
    '''

if __name__ == '__main__':
    starttime = time.clock()
    s = [int(x) for x in inp.readlines()[1].split()]
    i = len(s)

    ListIn = [(0,[])]*i #Longest seq (position) of each number
    ListDe = [(0,[])]*i
    i -= 1
    while i >= 0:
        LongSub(i)
        i -= 1

    for i in max(ListIn)[1]:
        outp.write(str(s[i])+' ')
    outp.write('\n')
    for i in max(ListDe)[1]:
        outp.write(str(s[i])+' ')
    timeused = time.clock() - starttime
    print timeused
inp.close()
outp.close()

## 20.8011832813 (new)

## 75.5757403779




'''
starttime = time.clock()
p= map(int, open("rosalind_lgis.txt").read().splitlines()[1].split() )

def insert( m, lis, bs):
    if (len(lis)==0) or (m>lis[-1]) :
        bs.append([m])
        return lis + [m]
    else:
        i=0
        for j in range(len(p)):
            if lis[j]>m:
                i = j
                break
        bs[i].append(m)
        lis[i] = m
        return lis

def find_mis(perm):
    tab = []
    basic_sequences=[]
    for x in perm:
        tab = insert(x,tab, basic_sequences)

    mis = [ basic_sequences.pop()[0] ]
    for j in range ( len (basic_sequences) ):
        next_basic_seq = basic_sequences.pop()
        u = [x for x in next_basic_seq if (x < mis[0]) and (p.index(x) < p.index(mis[0]))][0]
        mis = [u] + mis

    return mis

print " ".join(map(str, find_mis(p)))

timeused = time.clock() - starttime
print timeused
'''
## 3.77594060161





'''
inp = open('rosalind_lgis.txt')

starttime = time.clock()
l = [int(x) for x in inp.readlines()[1].split()]
n = len(l)
inc = [(0,[])]*(n+1)

for i in l:
    x,y = max(inc[:i])
    inc[i] = (x+1,y+[i])

print(" ".join(map(str,max(inc)[1])))

timeused = time.clock() - starttime
print timeused
'''
##5.88072905668

