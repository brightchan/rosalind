__author__ = 'Bright'
inp = open('rosalind_tree.txt','r')
outp = open('output.txt','w')

class Graph:
    def __init__(self, v):
        self.v = v
        self.adj = [[] for x in range(v)]
    def add_edge(self, v, w):
        self.adj[v].append(w)
        self.adj[w].append(v)
    def print_(self):
        for i in range(self.v):
            for j in self.adj[i]:
                print str(i) + '-' + str(j)
    def components(self):
        self.edge_to = [None for x in xrange(v)]
        self.marked = [False for x in xrange(v)]
        self.cc = [None for x in xrange(v)]
        global counter
        counter = 0
        for x in xrange(v):
            if not self.marked[x]:
                dfs(self,x)
                counter += 1
        return counter

def dfs(g,v):#recursive DFS for all nodes linked to v with marked[], cc[] and edge_to[]
    global counter
    g.marked[v] = True
    g.cc[v] = counter
    for i in g.adj[v]:
        if not g.marked[i]:
            dfs(g,i)
            g.edge_to[i] = v

if __name__ == "__main__":
    v = int(inp.readline().strip())
    tree = Graph(v)
    link = [int(x)-1 for x in inp.readline().strip().split()]
    while link:
        tree.add_edge(link[0],link[1])
        link = [int(x)-1 for x in inp.readline().strip().split()]
    print tree.components(),tree.cc


