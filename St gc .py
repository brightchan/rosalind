'''
f = open('rosalind_gc.txt', 'r')

max_gc_name, max_gc_content = '', 0

buf = f.readline().rstrip()
while buf:
    seq_name, seq = buf[1:], ''
    buf = f.readline().rstrip()
    while not buf.startswith('>') and buf:
        seq = seq + buf
        buf = f.readline().rstrip()
    seq_gc_content = (seq.count('C') + seq.count('G'))/float(len(seq))
    if seq_gc_content > max_gc_content:
        max_gc_name, max_gc_content = seq_name, seq_gc_content

print('%s\n%.6f%%' % (max_gc_name, max_gc_content * 100))
f.close()
'''

inp = open('D:/Google Drive/Rosalind/rosalind_gc.txt','r')

l = inp.read().split('>')
print l
del l[0]
d={}
d2={}
name=''
max=0.0
for i in range(len(l)):
    d[l[i][0:14]]=l[i][14:]
for k,v in d.items():
    d2[k]=(v.count('G')+v.count('C'))/float(v.count('G')+v.count('C') + v.count('A')+v.count('T'))
    print d2[k]
    if d2[k]>max:
        max=d2[k]
        name=k
print name,max*100
inp.close()

