inp = open('rosalind_fibd.txt','r')
outp = open('output.txt','w')
m,n = (int(x) for x in inp.readline().strip().split(' '))
newborn = [0,1,0]
living = [0,1,1]
for i in range(3,n+1):
    newborn.append(living[i-2])
    living.append(living[i-1] + newborn[i])
for i in range(n+1,m+1):
    newborn.append(living[i-2]-newborn[i-n-1])
    living.append(living[i-1]+newborn[i]-newborn[i-n])
print living[m]
inp.close()
outp.close()

def fib(m,n):
  ages = [1] + [0]*(n-1)
  for i in xrange(m-1):
    ages = [sum(ages[1:])] + ages[:-1]
  return sum(ages)

print fib(m,n)

