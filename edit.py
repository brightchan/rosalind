# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 15:30:28 2015

@author: Bright
"""

from Tools import lcsq,read_FASTA

with open('rosalind_edit.txt','r') as inp:
    seq = read_FASTA(inp)

s1,s2 = seq[0],seq[1]

M = [[0]*(len(s2)+1) for i in range(len(s1)+1)]

for i in range(1,len(s1)+1):
    M[i][0] = i
for j in range(1,len(s2)+1):
    M[0][j] = j

for i in range(len(s1)+1):
    for j in range(len(s2)+1):
        if s1[i-1] == s2[j-1]:
            M[i][j] = M[i-1][j-1]
        else:
            M[i][j] = min(M[i-1][j-1],M[i-1][j],M[i][j-1])+1

print(M[len(s1)][len(s2)])