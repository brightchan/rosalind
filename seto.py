__author__ = 'Bright'
inp = open('rosalind_seto.txt','r')

c = set(range(1,int(inp.readline().strip())+1,1))
a = set(int(i) for i in inp.readline().strip('{}\n').split(', '))
b = set(int(i) for i in inp.readline().strip('{}\n').split(', '))

uab = '{' + ', '.join(str(i) for i in sorted(a|b)) + '}'
iab = '{' + ', '.join(str(i) for i in sorted(a&b)) + '}'
a_b = '{' + ', '.join(str(i) for i in sorted(a-b)) + '}'
b_a = '{' + ', '.join(str(i) for i in sorted(b-a)) + '}'
c_a = '{' + ', '.join(str(i) for i in sorted(c-a)) + '}'
c_b = '{' + ', '.join(str(i) for i in sorted(c-b)) + '}'
print uab
print iab
print a_b
print b_a
print c_a
print c_b