# Rosalind code scripts #

Python scripts for the rosalind project. Rosalind is a platform for learning bioinformatics through problem solving. Each script answers one problem.  [http://rosalind.info/problems/locations/](Link URL)

By CJB since 2015