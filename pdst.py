__author__ = 'Bright'
inp = open('rosalind_pdst.txt','r')
from Tools import *

def dist(seq1,seq2):
    diff = 0
    for i in range(len(seq1)):
        if seq1[i] <> seq2[i]:
            diff += 1
    return float(diff)/len(seq1)

seq = read_FASTA(inp)
distance_matrix = [[0.0 for j in range(len(seq))] for i in range(len(seq))]
for i in range(len(seq)):
    for j in range(i+1,len(seq),1):
        distance_matrix[i][j] = dist(seq[i],seq[j])
        distance_matrix[j][i] = distance_matrix[i][j]
for i in range(len(seq)):
    for j in range(len(seq)):
        print ('%1.5f' % (distance_matrix[i][j])),
    print ''