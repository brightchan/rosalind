__author__ = 'Bright'
inp = open('rosalind_kmp.txt','r')
import time
st = time.clock()
inp.readline()
seq = ''.join([s.strip() for s in inp.readlines()])
fail_arr = [0 for i in xrange(len(seq))]
i = 1
while i < len(seq):
    for length in range(1,len(seq)-i,1):
        if seq[:length] <> seq[i:i+length]:
            break
        else:
            if fail_arr[i+length-1] < length:
                fail_arr[i+length-1] = length
    i += 1
o = ''
for i in fail_arr:
    o += str(i) + ' '
print o
f = time.clock()
print f-st
inp.close()

# never try directly. think how to use existing numbers
'''
failure=[0] * len(s)
for i in range(1,len(s)):
    s1=s[0:i+1]
    f=failure[i-1]+1
    while f > 0:
        if s1[0:f]==s1[-f:]:
            failure[i]=f
            f=0
        else:
            f-=1
'''