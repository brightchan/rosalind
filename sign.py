outp = open('output.txt','w')

Permnum = 0
Perm = []

def PermRecur(n,arrl,reml):
    global Permnum
    if reml:
        for i in range(len(reml)):
            a = reml[i]
            if n == 1:
                temp1 = [i for i in arrl]
                temp1.append(a)              
                for i in sign:
                    temp3 = []
                    for j in xrange(len(temp1)):
                        temp3.append(''.join([i[j],temp1[j]]))
                    Perm.append(temp3)
                    Permnum += 1
            elif n > 1:
                temp1 = [i for i in arrl]
                temp1.append(a)
                temp2 = [i for i in reml]
                temp2.remove(a)
                PermRecur(n-1,temp1,temp2)

def SignRecur(n,arrl):
    global sign
    leng = n - len(arrl)
    temp1 = [i for i in arrl]
    for i in xrange(leng):
        temp1.append('')
    if leng > 1:
        SignRecur(n,temp1[:-(leng-1)])
    temp1[len(arrl)] = '-'
    if not temp1 in sign:
        sign.append(temp1)
        if leng > 1:
            SignRecur(n,temp1[:-(leng-1)])   
  
             
if __name__ =='__main__':
    l = []
    temp = []
    sign = []
    num = 6
    for i in xrange(num):
        l.append(str(i+1))
        temp.append('')
    sign.append(temp)
    SignRecur(num,[])
    PermRecur(num,[],l)
    outp.write(str(Permnum)+'\n')
    for i in Perm:
        outp.write(' '.join(i)+'\n')


outp.close()
