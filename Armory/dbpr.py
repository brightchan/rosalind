# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 02:20:41 2015

@author: Bright
"""

from Bio import ExPASy
from Bio import SwissProt
handle = ExPASy.get_sprot_raw('P46067')
try:
    record = SwissProt.read(handle)
except ValueException:
    print('Target not found.')  
for i in record.cross_references:
    if i[0] == 'GO' and 'P:' in i[2]:
        print(i[2][2:])

