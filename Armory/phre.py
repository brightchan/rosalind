# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 22:15:39 2015

@author: Bright
"""
from Bio import SeqIO
from statistics import mean
from timeit import timeit

def mine():
    with open('rosalind_phre.txt') as f:
        n = int(f.readline().strip())
        records = SeqIO.parse(f,'fastq')
        count = [record.id for record in records if mean(record.letter_annotations['phred_quality'])<n]

def loop():
    with open('rosalind_phre.txt') as f:
        n = int(f.readline().strip())
        records = SeqIO.parse(f,'fastq')
        count = 0
        for record in records:
            if mean(record.letter_annotations['phred_quality'])<n:
                count +=1

print(timeit(mine,number=10))
print(timeit(loop,number=10))
