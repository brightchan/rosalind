# -*- coding: utf-8 -*-
"""
Created on Sun Jul  5 15:54:28 2015

@author: Bright
"""
from Bio import SeqIO

f = open('rosalind_bfil.txt','r')
out = open('output.txt','w')
n = int(f.readline().strip())
records = SeqIO.parse(f,'fastq')
for record in records:
    qual = record.letter_annotations["phred_quality"]    
    l,r = 0,len(qual)
    for i in range(0,len(qual)):
        if qual[i]<n:
            l = l+1
        else:
            break
    for i in range(len(qual)-1,-1,-1):
        if qual[i]<n:
            r = r-1
        else:
            break
    SeqIO.write(record[l:r],out,'fastq')
f.close()
out.close()
