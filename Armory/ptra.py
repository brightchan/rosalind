# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 18:11:06 2015

@author: Bright
"""

from Bio.Seq import translate

with open('rosalind_ptra.txt') as f:
    dnaseq = f.readline().strip()
    prtseq = f.readline().strip()
    for i in range(1,16,1):
        if i != 7 and i != 8:
            if prtseq == translate(dnaseq, table=i, to_stop=True):
                print(i)

        
    