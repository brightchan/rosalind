# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 21:36:59 2015

@author: Bright
"""

from Bio import SeqIO
from numpy import mean
from timeit import timeit

def qual_of_base():
    with open('rosalind_bphr.txt') as f:
        n = int(f.readline().strip())
        records = list(SeqIO.parse(f,'fastq'))
        leng = len(records[0].seq)
        l = len(records)
        count = 0
        for i in range(leng):
            if mean([record.letter_annotations["phred_quality"][i] for record in records])<n:
                count +=1
        print(count)

def o():
    handle = open('rosalind_bphr.txt','r')
    limit = float(handle.readline())
    records = list(SeqIO.parse(handle, "fastq"))
    count = 0
    for i in range(0, len(records[0].seq)):
        sum = 0
        for j in range(0, len(records)):
            sum += records[j].letter_annotations["phred_quality"][i]
        if sum/len(records) < limit:
            count += 1
    print(count)

print(timeit(qual_of_base,number=100))
print(timeit(o,number=100))