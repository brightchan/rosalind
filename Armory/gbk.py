# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 18:03:51 2015

@author: Bright
"""

from Bio import Entrez as Enz

with open('rosalind_gbk.txt') as f:
    kw = f.read().splitlines()
print(kw)
       
handle = Enz.esearch(db='nucleotide%s AND %s:%s [Publication Date]', 
                    % (term=kw[0],kw[1],kw[2]))
record = Enz.read(handle)
print(record['Count'])