def findORF(rna):
    dic = {}
    for i in xrange(len(rna)):
        if rna[i:i+3] == 'ATG':
            for j in xrange(i+3,len(rna)+1,3):
                if rna[j:j+3] == 'TAA' or rna[j:j+3] == 'TAG' or rna[j:j+3] == 'TGA':
                    dic[translate(rna[i:j])] = 1
                    break
    return dic


def translate(rna):
    s = ''
    for i in range(0,len(rna),3):
        s += d[rna[i:i+3]]
    return s



def revcom(DNA):
    l = ''
    for i in range(len(DNA)-1,-1,-1):
        if DNA[i] == 'A':
            l +=('T')
        if DNA[i] == 'T':
            l +=('A')
        if DNA[i] == 'C':
            l +=('G')
        if DNA[i] == 'G':
            l +=('C')
    return l

def lcsq(s1,s2):
    cur = [''] * (len(s2) + 1) #dummy entries as per wiki
    for s in s1:
        last, cur = cur, [''] 
        for i, t in enumerate(s2):
            cur.append(last[i] + s if s==t else max(last[i+1], cur[-1], key=len))
    return cur[-1]

def read_FASTA(Inp):#return only seq w/o names in a list
    seq = []
    i = -1
    buf = Inp.readline().rstrip()
    while buf:
        if buf.startswith('>'):
            seq.append('')
            i += 1
            buf = Inp.readline().rstrip()
        else:
            seq[i] += buf
            buf = Inp.readline().rstrip()
    return seq

def FASTAread(Inp):#return a dict with all seqs with names
    d = {}
    buf = Inp.readline().rstrip()
    while buf:
        name, d[buf[1:]] = buf[1:], ''
        buf = Inp.readline().rstrip()
        while not buf.startswith('>') and buf:
            d[name] += buf
            buf = Inp.readline().rstrip()
    return d


def longestCommon(seqs):
    shortest = min(seqs, key=len)
    for length in xrange(len(shortest), 0, -1):
        for start in xrange(len(shortest) - length + 1):
            sub = shortest[start:start+length]
            if all(seq.find(sub) >= 0 for seq in seqs):
                return sub
    return ""

def hamm_dist(s1,s2):
    return len([x for x, y in zip(s1,s2) if x != y])
