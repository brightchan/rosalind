# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 02:30:21 2015

@author: Bright
"""
from timeit import timeit
from Tools import revcom,read_FASTA

def m():
    with open('rosalind_rvco.txt') as f:
        seq = read_FASTA(f)
        c = [i for i in seq if revcom(i) == i]
        


from Bio import SeqIO

def o():
    with open('rosalind_rvco.txt') as f:
        c = len([rec for rec in SeqIO.parse(f, "fasta")
           if str(rec.seq) == str(rec.reverse_complement().seq)])
               
print(timeit(m,number=100))
print(timeit(o,number=100))