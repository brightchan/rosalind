# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 21:04:33 2015

@author: Bright
"""

from Bio import Entrez as Enz
from Bio import SeqIO

Enz.email = 'chenjb7@msn.com'
with open('rosalind_frmt.txt') as f:
    ID = f.readline().split()
handle = Enz.efetch(db='nucleotide', id = ID, rettype='fasta')
records = list(SeqIO.parse(handle,'fasta'))

l = [0,len(records[0].seq)]
for i in range(1,len(records),1):
    if len(records[i].seq) < l[1]:
        l[0] = i
        l[1] = len(records[i].seq)
print('>'+records[l[0]].description+'\n'+records[l[0]].seq)