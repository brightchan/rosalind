inp = open('rosalind_mrna.txt','r')
outp = open('output.txt','w')
codon = open('codon.txt','r')
Protein = inp.read().strip()
c = codon.read().strip().split()
dc = {}
dn = {} #count the possibility of aa
s = 3
dc = dict(zip(c[0::2],c[1::2]))

for k in dc:
    if dc[k] in dn:
        dn[dc[k]] +=1
    else:
        dn[dc[k]] = 1

for i in xrange(len(Protein)):
    s *= dn[Protein[i]]
    if s > 1000000:
        s = s % 1000000

print s

inp.close()
outp.close()
codon.close()
