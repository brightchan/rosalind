__author__ = 'Bright'
import time
from Tools import FASTAread
inp = open('rosalind_long.txt')
outp = open('output.txt','w')

def overlap(seq1,seq2):#longer one also in seq1
    for i in range(len(seq2),0,-1):
        if seq2[:i] == seq1[-i:]:
            return i
        if seq2[-i:] == seq1[:i]:
            return -i

def merge(seq1,seq2):#produce superstring with longer one put in seq1
    L = overlap(seq1,seq2)
    if L > 0:
        m = seq1 + seq2[L:]
    else:
        m = seq2[:L] + seq1
    return m

if __name__ == '__main__':
    starttime = time.clock()
    s = FASTAread(inp)
    seq = []
    for k,v in s.items():
        seq.append(v)
    mer = seq[0]
    seq.remove(mer)
    c = 0
    while seq:
        for i in seq:
            if i[:len(i)/2] in mer or i[len(i)/2:] in mer:
                mer = merge(mer,i)
                seq.remove(i)
                print c
                c += 1
    print mer
    timeused = time.clock() - starttime
    print timeused

inp.close()
outp.close()