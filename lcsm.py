import time
inp = open('rosalind_lcsm.txt','r')
outp = open('output.txt','w')
d = {}
num = 0 #num of seq
#dict of seq
buf = inp.readline().rstrip()
while buf:
    name, d[buf[1:]] = buf[1:], ''
    num += 1
    buf = inp.readline().rstrip()
    while not buf.startswith('>') and buf:
        d[name] += buf
        buf = inp.readline().rstrip()
#creat seed by increasing length while indexing and comparing
seed = {}
def find(d,l):
    cons = ''
    for k,v in d.items():
        for i in xrange(len(v)-l):
            if not (v[i:i+l] in seed):
                print v[i:i+l]+'\n'
                for k2,v2 in d.items():
                    if k2 != k:
                        j = 0
                        while j<len(v2):
                            if v[i:i+l] == v2[j:j+l]:
                                seed[v[i:i+l]] += 1
                                break
                            j += 1
                if seed[v[i:i+l]] == num-1:
                    cons = v[i:i+l]
                    return cons
def increase(l):
    s,con = '',''
    while l < 1000:
        s = find(d,l)
        print s
        if len(s)<l:
            return con
        else:
            con = s
        l += 1

starttime = time.clock()
increase(2)
timeused = time.clock() - starttime
print timeused

inp.close()
outp.close()

'''
def longestCommon(seqs):
  shortest = min(seqs, key=len)
  for length in xrange(len(shortest), 0, -1):
    for start in xrange(len(shortest) - length + 1):
      sub = shortest[start:start+length]
      if all(seq.find(sub) >= 0 for seq in seqs):
        return sub
  return ""
'''
