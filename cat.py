__author__ = 'Bright'
def cat_num(n):#return the cat num of G with 2n nodes
    c = [0,0]
    c[0] = 1
    c[1] = 1
    i = 2
    while i <= n:
        c.append(sum(c[k]*c[i-k-1] for k in range(i)))
        i+=1
    return c[n]

print(cat_num(50))