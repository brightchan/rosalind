outp = open('output.txt','w')

Permnum = 0
Perm = []

def PermRecur(n,arrl,reml):
    global Permnum
    if reml:
        for i in range(len(reml)):
            a = reml[i]
            if n == 1:
                temp1 = [i for i in arrl]
                temp1.append(a)
                Perm.append(' '.join(temp1))
                Permnum += 1
            elif n > 1:
                temp1 = [i for i in arrl]
                temp1.append(a)
                temp2 = [i for i in reml]
                temp2.remove(a)
                PermRecur(n-1,temp1,temp2)

             
if __name__ =='__main__':
    l = []
    num = 5
    for i in xrange(num):
        l.append(str(i+1))
    PermRecur(num,[],l)
    print Permnum
    for i in Perm:
        print i


outp.close()

