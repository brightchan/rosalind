def revcom(DNA):
    l = ''
    for i in range(len(DNA)-1,-1,-1):
        if DNA[i] == 'A':
            l +=('T')
        if DNA[i] == 'T':
            l +=('A')
        if DNA[i] == 'C':
            l +=('G')
        if DNA[i] == 'G':
            l +=('C')
    return l

def FindRevcom(s1,s2):
    s2 = revcom(s2)
    shortest = min(s1,s2)
    for length in xrange(len(shortest), 0, -1):
        for start in xrange(len(shortest) - length + 1):
            sub = shortest[start:start+length]
            if s1.find(sub) >= 0 and s2.find(sub) >=0:
                return sub
    return ""

a = 'GGCCGCAGCGTAATCCT'
b = 'CCGTTACGCTAAATTAT'

print FindRevcom(a,b)